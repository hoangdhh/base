package base.mobile.hd.com.base.base.activity;

import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import base.mobile.hd.com.base.R;
import base.mobile.hd.com.base.base.fragment.BaseFragment;
import base.mobile.hd.com.base.global.MenuType;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.internal.Utils;

public class BaseActivity extends AppCompatActivity implements View.OnClickListener {

    @BindView(R.id.ic_navi)
    ImageView imgNavi;
    @BindView(R.id.ll_navi)
    LinearLayout llNavi;
    @BindView(R.id.titleToolbar)
    TextView titleToolbar;
    @BindView(R.id.tv_edit)
    TextView tvEdit;
    @BindView(R.id.ll_edit_text)
    LinearLayout llEditText;
    @BindView(R.id.img_help)
    ImageView imgHelp;
    @BindView(R.id.ll_edit_image)
    LinearLayout llEditImage;
    @BindView(R.id.toolbar)
    Toolbar toolBar;
    @BindView(R.id.app_bar_layout)
    AppBarLayout appBarLayout;
    @BindView(R.id.frame_content)
    FrameLayout frameContent;
    @BindView(R.id.drawer_layout)
    DrawerLayout drawerLayout;

    private FragmentManager fragmentManager;
    private NavigationView mNavigationView;
    private MenuType mMenuType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base);
        fragmentManager = getSupportFragmentManager();
    }

    protected void setContentToBase(int idView) {
        FrameLayout mContent = (FrameLayout) findViewById(R.id.frame_content);
        mContent.removeAllViews();
        mContent.addView(LayoutInflater.from(this).inflate(idView, mContent, false));
        ButterKnife.bind(this);
        llNavi.setOnClickListener(this);
    }

/*    *//* set show tool bar*//*
    protected void showToolBar(){
        appBarLayout.setVisibility(View.VISIBLE);
    }*/

    protected void showToolBar(MenuType type) {
        this.mMenuType = type;
        appBarLayout.setVisibility(View.VISIBLE);
        switch (type) {
            case BACK:
                imgNavi.setImageResource(R.drawable.ic_arrow_back);
                break;
            case NAVIGATION:
                initNavigation();
                break;
            default:
                break;
        }
    }

    protected void setEnabelNavigation(boolean isEnabel) {
        if (!isEnabel) {
            drawerLayout.removeAllViews();
            return;
        }
        initNavigation();
    }

    /*set title tool bar*/
    protected void setTitleToolbar(String title) {
        titleToolbar.setText(title);
    }

    /*init menu*/
    protected void initNavigation() {
        View view = LayoutInflater.from(this).inflate(R.layout.layout_menu, drawerLayout, false);
        mNavigationView = (NavigationView) view;
        if (mNavigationView == null) return;
        mNavigationView.setVisibility(View.VISIBLE);
        ViewGroup.LayoutParams lp = mNavigationView.getLayoutParams();
        drawerLayout.addView(mNavigationView, 1, lp);
    }


    protected void changeFragment(BaseFragment fragment) {
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.frame_content, fragment, BaseActivity.class.getName()).commit();
    }

    @Override
    public void onClick(View view) {
        if (view == null) return;
        switch (view.getId()) {
            case R.id.ll_navi:
                switch (mMenuType) {
                    case NAVIGATION:
                        drawerLayout.openDrawer(GravityCompat.START);
                        break;
                    case BACK:
                        onBackPressed();
                        break;
                }
                break;
            case R.id.ll_edit_image:
                break;
            case R.id.ll_edit_text:
                break;
        }
    }
}
