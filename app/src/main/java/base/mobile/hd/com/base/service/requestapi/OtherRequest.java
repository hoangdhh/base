package base.mobile.hd.com.base.service.requestapi;

import base.mobile.hd.com.base.service.listenerapi.OnResponse;
import base.mobile.hd.com.base.service.response.object.ResponseObj;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by hoang do on 12/15/2017.
 */

public class OtherRequest extends ApiRequest {

    private static final String MULTIPART_FORM_DATA = "multipart/form-data";

    public static OtherRequest getInstance() {
        initApi();
        return new OtherRequest();
    }

    /**
     * Get List Provider / Top-up
     *
     * @param mResponse {@link ResponseObj}
     */
    public void getListProvider(final OnResponse<String, ResponseObj> mResponse) {
        mResponse.onStart();
        Call<ResponseBody> call = mService.getNoti();
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                checkResponse(response, call, mResponse, "");
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                getResponseWhenError(mResponse, call, t, "");
            }
        });
    }
}
