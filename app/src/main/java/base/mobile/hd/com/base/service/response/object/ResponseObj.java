package base.mobile.hd.com.base.service.response.object;

import org.json.JSONObject;

/**
 * Created by hoang do on 12/15/2017.
 */

public class ResponseObj {
    private String code = "";
    private String msg = "";
    private JSONObject jsonObject;

    public ResponseObj(String errorCode, String mess) {
        this.code = errorCode;
        this.msg = mess;
    }

    public ResponseObj(JSONObject jsonObject) {
        this.jsonObject = jsonObject;
    }

    public ResponseObj() {
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public JSONObject getJsonObject() {
        return jsonObject;
    }

    @Override
    public String toString() {
        return "ResponseObj{" +
                "code='" + code + '\'' +
                ", msg='" + msg + '\'' +
                '}';
    }
}
