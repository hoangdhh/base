package base.mobile.hd.com.base.home.activity;

import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import base.mobile.hd.com.base.R;
import base.mobile.hd.com.base.base.activity.BaseActivity;
import base.mobile.hd.com.base.global.MenuType;
import base.mobile.hd.com.base.object.response.TempDemoObject;
import base.mobile.hd.com.base.service.jsonparser.JsonParser;
import base.mobile.hd.com.base.service.listenerapi.OnResponse;
import base.mobile.hd.com.base.service.requestapi.OtherRequest;
import base.mobile.hd.com.base.service.response.object.ResponseObj;
import butterknife.BindView;

public class HomeActivity extends BaseActivity {

    @BindView(R.id.tv_hello)
    TextView tvHello;
    @BindView(R.id.tv_hello_detail)
    TextView tvHelloDetail;

    private TempDemoObject tempDemoObject = new TempDemoObject();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentToBase(R.layout.activity_home);
        showToolBar(MenuType.NAVIGATION);
        setTitleToolbar("Home");
    }

    /*private void getDataFromServer() {
        OtherRequest.getInstance().getListProvider(
                new OnResponse<String, ResponseObj>() {
                    @Override
                    public void onResponseSuccess(String tag, String rs, ResponseObj extraData) {
                        Log.e("data", extraData.getJsonObject().toString());
                        tempDemoObject = JsonParser.getTempDemo(extraData.getJsonObject().toString());
                        tvHelloDetail.setText(tempDemoObject.getBody());
                        tvHello.setText(tempDemoObject.getTitle());
                    }

                    @Override
                    public void onResponseError(String tag, String message) {
                        Log.e("eror", message);
                    }
                }
        );
    }*/
}
