package base.mobile.hd.com.base.service.requestapi;

import android.util.Log;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.JsonWriter;

import org.json.JSONObject;

import java.io.IOException;
import java.util.concurrent.TimeUnit;


import base.mobile.hd.com.base.service.listenerapi.OnResponse;
import base.mobile.hd.com.base.service.response.object.ResponseObj;
import base.mobile.hd.com.base.config.Config;
import okhttp3.OkHttpClient;
import okhttp3.Response;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by hoangdo on 15/12/17.
 */

public class ApiRequest {

    public static final String TAG = "ApiRequest";
    protected Retrofit mRetrofit;
    protected ApiService mService;

    protected static void initApi() {
        new ApiRequest();
    }

    public Gson initGson() {
        return new GsonBuilder()
                .disableHtmlEscaping()
                .registerTypeAdapter(Boolean.class, booleanAsIntAdapter)
                .registerTypeAdapter(boolean.class, booleanAsIntAdapter)
                .registerTypeAdapter(String.class, stringAsObjectAdapter)
                .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
                .setPrettyPrinting()
                .serializeNulls()
                .create();
    }

    private TypeAdapter<Boolean> booleanAsIntAdapter = new TypeAdapter<Boolean>() {
        @Override
        public void write(JsonWriter out, Boolean value) throws IOException {
            if (value == null) {
                out.nullValue();
            } else {
                out.value(value);
            }
        }

        @Override
        public Boolean read(JsonReader in) throws IOException {
            JsonToken peek = in.peek();
            switch (peek) {
                case BOOLEAN:
                    return in.nextBoolean();
                case NULL:
                    in.nextNull();
                    return null;
                case NUMBER:
                    return in.nextInt() != 0;
                case STRING:
                    return Boolean.parseBoolean(in.nextString());
                default:
                    throw new IllegalStateException("Expected BOOLEAN or NUMBER but was " + peek);
            }
        }
    };

    private TypeAdapter<String> stringAsObjectAdapter = new TypeAdapter<String>() {
        @Override
        public void write(JsonWriter out, String value) throws IOException {
            if (value == null) {
                out.nullValue();
            } else {
                out.value(value);
            }
        }

        @Override
        public String read(JsonReader in) throws IOException {
            return in.toString();
        }
    };

    public ApiRequest() {
        OkHttpClient httpClient = new OkHttpClient().newBuilder()
                .readTimeout(60, TimeUnit.SECONDS)
                .connectTimeout(60, TimeUnit.SECONDS)
                .writeTimeout(60, TimeUnit.SECONDS)
                .build();

        mRetrofit = new Retrofit.Builder()
                .client(httpClient)
                .baseUrl(Config.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(initGson()))
                .build();
        mService = mRetrofit.create(ApiService.class);
    }

    /**
     * Get Response when success
     *
     * @param response  {@link Response < ResponseBody >}
     * @param call      {@link Call <ResponseBody>}
     * @param mResponse {@link OnResponse}
     * @param tag       name of API
     */
    void checkResponse(retrofit2.Response<ResponseBody> response, Call<ResponseBody> call, OnResponse<String, ResponseObj> mResponse, String tag) {
        ResponseObj responseObj = checkValidResponse(response);
        if (!responseObj.getMsg().isEmpty()) {
            Log.d(ApiRequest.class.getSimpleName(), tag + "/" + responseObj.getMsg() + "/" + responseObj.getCode());
            mResponse.onResponseError(tag, responseObj.getMsg());
            mResponse.onFinish();
            return;
        }
        try {
            mResponse.onResponseSuccess(tag,
                    "",
                    responseObj);
        } catch (Exception e) {
            e.printStackTrace();
            mResponse.onFinish();
            return;
        }
        mResponse.onFinish();
    }

    /**
     * Get Response when fail
     *
     * @param mResponse {@link Response<ResponseBody>}
     * @param call      {@link Call<ResponseBody>}
     * @param throwable {@link Throwable}
     * @param tag       name of API
     */
    void getResponseWhenError(OnResponse<String, ResponseObj> mResponse, Call<ResponseBody> call, Throwable throwable, String tag) {
        mResponse.onResponseError(tag, throwable.getMessage());
        mResponse.onFinish();
    }

    /**
     * Check valid response
     *
     * @param response {@link Response<ResponseBody>}
     * @return depend on HTTP Code response and valid header signature
     */
    private ResponseObj checkValidResponse(retrofit2.Response<ResponseBody> response) {
        JSONObject jsonObject = new JSONObject();
        try {
//            if (response.code() == HTTP_CODE_UNAUTHORIZED) {
//                return new ResponseObj(ResponseKeyCode.ERROR_ACCESS_TOKEN_EXPIRED.getErrorCode(), ResponseKeyCode.ERROR_ACCESS_TOKEN_EXPIRED.getErrorExplain());
//            }
            //no inspection ConstantConditions
            jsonObject = new JSONObject(response.body().string());
//            if (response.code() != HTTP_CODE_SUCCESS) {
//                return getError(response);
//            }
//            if (!VerifySignature.checkSignature(response.headers().get(HEADER_SIGNATURE))) {
//                return new ResponseObj(Integer.toString(response.code()), "Verify key error");
//            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ResponseObj(jsonObject);
    }

}
