package base.mobile.hd.com.base.service.requestapi;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface ApiService {


    String API_POST_CITY = "api/CustomerCredit/GetListCity";
    String API_GET_NOTI = "posts/1";

    @Headers({"Cache-Control: max-age=640000"})
    @GET(API_POST_CITY)
    Call<ResponseBody> postCity(@Body String _result);

    @Headers({"Cache-Control: max-age=640000"})
    @GET(API_GET_NOTI)
    Call<ResponseBody> getNoti();

}
