package base.mobile.hd.com.base.service.jsonparser;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import base.mobile.hd.com.base.object.response.TempDemoObject;

/**
 * Created by hoangdo
 * on 5/18/2017.
 */

public class JsonParser {

    public static final Gson gson = new GsonBuilder()
            .setExclusionStrategies(new ExclusionStrategy() {
                @Override
                public boolean shouldSkipField(FieldAttributes f) {
                    return false;
                }

                @Override
                public boolean shouldSkipClass(Class<?> clazz) {
                    return false;
                }
            })
            .disableHtmlEscaping()
            .create();

    public static TempDemoObject getTempDemo(String data) {
        try {
            return gson.fromJson(data, TempDemoObject.class);
        } catch (Exception e) {
            e.printStackTrace();
            return new TempDemoObject();
        }
    }

    public static List<TempDemoObject> getListTempDemo(String data) {
        try {
            Type type = new TypeToken<List<TempDemoObject>>() {
            }.getType();
            return gson.fromJson(data, type);
        } catch (Exception e) {
            e.printStackTrace();
            return new ArrayList<>();
        }
    }
}
